/**
 * 
 */
package exa.com.springbootdemo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import exa.com.services.StudentService;

/**
 * @author Ravikiran J
 *
 */
@EnableAutoConfiguration // it will auto configure what we used in the classpath
@ComponentScan({"exa.com"}) //  scans for package what we provide
public class StudentControllerTest extends SpringBootDemoApplicationTests {

	@Autowired
	private WebApplicationContext webApplicationContext;
	//Interface to provide configuration for a web application. This is read-only while the application is running, but may be reloaded if the implementation supports this

	private MockMvc mockMvc;

	@Autowired
	private StudentService studentServ;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	//student-add page check test
	@Test
	public void testIndex() throws Exception{
		this.mockMvc.perform(get("/customer-add"))
		.andExpect(status().isOk())
		.andExpect(view().name("customer-add"))
		.andDo(print());
	}

	//student list check test
	@Test
	public void testList() throws Exception {
		assertThat(this.studentServ).isNotNull();
		mockMvc.perform(MockMvcRequestBuilders.get("/student-list"))
		.andExpect(status().isOk()) //return 200(success status) if status is true
		.andExpect(content().string(Matchers.containsString("")))//check given string exist or not
		.andDo(print());//in console it gives junit information
	}

	//student list check test
	/*@Test
	public void testSave() throws Exception {
		assertThat(this.studentServ).isNotNull();
		mockMvc.perform(MockMvcRequestBuilders.post("/save").contentType(MediaType.APPLICATION_JSON)
			    .content("{\"json\":\"request to be send\"}"))
		.andExpect(status().isOk()) //return 200 if status is true
		.andExpect(content().string(Matchers.containsString("asd")))//check given string exist or not
		.andDo(print());//in console it gives junit information
	}*/



}
