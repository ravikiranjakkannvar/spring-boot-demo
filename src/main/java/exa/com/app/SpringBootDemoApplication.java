package exa.com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({"exa.com"}) // controller scans for package what we provide
@SpringBootApplication
@EnableJpaRepositories({"exa.com"})
@EntityScan("exa.com")
@EnableAutoConfiguration
public class SpringBootDemoApplication extends SpringBootServletInitializer {

	/**
	 * Allows you to configure your application 
	 * when it’s launched by the servlet container
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootDemoApplication.class);
	}

	/*
	 *Its a main method to check boot 
	 *working fine or not ! 
	 */


	public static void main(String[] args) {

		SpringApplication.run(SpringBootDemoApplication.class, args);
	}

}
