/**
 * 
 */
package exa.com.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import exa.com.models.Student;
import exa.com.repositories.StudentRepository;


/**
 * @author Ravikiran J
 *
 */
@Service
public class StudentService {


	@Autowired
	private StudentRepository repo;

	//save
	public Student save(Student student){
		
		return repo.save(student);
	}

	//update
	public Student update(Student student){
		return repo.save(student);
	}

	//list
	public List<Student> list(){
		return repo.findAll();
	}

	//find one
	public Student findOne(int id) {
		return repo.findOne(id);
	}

	

}
