/**
 * 
 */
package exa.com.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import exa.com.models.Employee;
import exa.com.repositories.EmployeeRepository;
import exa.com.repositories.StudentRepository;


/**
 * @author Ravikiran J
 *
 */
@Service
public class EmployeeService{


	@Autowired
	private EmployeeRepository repo;

	//save
	public Employee save(Employee employee){
		return repo.save(employee);
	}

	//list
	public List<Employee> list(){
		return repo.findAll();
	}


	public Employee findByName(String name) {
		return repo.findByName(name);
	}

}
