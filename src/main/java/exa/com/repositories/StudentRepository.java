/**
 * 
 */
package exa.com.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import exa.com.models.Student;

/**
 * @author Ravikiran J
 *
 */
@Repository
public interface StudentRepository extends JpaRepository<Student,Integer > {

}
