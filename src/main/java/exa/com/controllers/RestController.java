package exa.com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import exa.com.models.Customer;
import exa.com.models.Response;
import exa.com.models.Student;
import exa.com.services.StudentService;

@org.springframework.web.bind.annotation.RestController
public class RestController {

	@Autowired
	private StudentService studentServ;
	
	List<Customer> cust = new ArrayList<Customer>();

	@RequestMapping(value = "/getallcustomer", method = RequestMethod.GET)
	public Response getResource() {
		Response response = new Response("Done", cust);
		return response;
	}

	@RequestMapping(value = "/postcustomer", method = RequestMethod.POST)
	public Response postCustomer(@RequestBody Customer customer) {
		cust.add(customer);
		// Create Response Object
		Response response = new Response("Done", customer);
		return response;
	}


	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public Customer firstPage() {
		Customer emp = new Customer();
		emp.setFirstname("raj");
		emp.setLastname("kiran");
		return emp;
	}
	
	


}
