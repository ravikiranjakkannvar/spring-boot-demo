/**
 * 
 */
package exa.com.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import exa.com.models.Course;
import exa.com.services.CourseService;

/**
 * @author Ravikiran J
 *
 */
@RestController
public class CourseController {

	Logger logger=LoggerFactory.getLogger(CourseController.class);

	@Autowired
	private CourseService courseService;

	@RequestMapping("/welcome")
	public String showMsg() {
		return "Hi Course";
	}

	//gettting list
	@GetMapping("/courses")
	public List<Course> getList(){
		logger.info("Getting List from Controller : "+this.getClass());
		return courseService.getList();
	}

	//save
	@PostMapping("/courses")
	public Course save(@RequestBody Course course) {
		logger.info("Save method at controller : "+this.getClass());
		return courseService.save(course);

	}

	//get one
	@GetMapping("/course/{id}")
	public Course getOne(@PathVariable Integer id){
		logger.info("Finding one List from Controller : "+this.getClass());
		return courseService.findOne(id);
	}

	//delete one
	@DeleteMapping("/course/{id}")
	public boolean delete(@PathVariable Integer id){
		logger.info("deleting One from Controller : "+this.getClass());
		Course course=courseService.findOne(id);
		if(course!=null)
		{
			courseService.delete(id);
			return true;
		}
		else {
			return false;
		}

	}

	//update
	@PutMapping("/courses/{id}")
	public boolean update(@RequestBody Course course,@PathVariable Integer id){
		logger.info("Updating One from Controller : "+this.getClass());


		Course course1=courseService.findOne(id);
		if(course1!=null)
		{
			courseService.update(id,course);
			return true;
		}
		else {
			return false;
		}
	}


}
