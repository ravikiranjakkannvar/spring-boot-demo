/**
 * 
 */
package exa.com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import exa.com.topics.Topic;
import exa.com.topics.TopicService;

/**
 * @author Ravikiran J
 *
 */
@RestController
public class TopicController {

	@Autowired
	private TopicService topicService;

	@GetMapping(value="/topics")
	public List<Topic> getList(){
		return topicService.getAllTopics();
	}

	
	@GetMapping(value="/topic/{id}")
	public Topic gettopic(@PathVariable String id){
		return topicService.getTopic(id);
	}
	
	@PostMapping(value="/topics")
	public void addtopic(@RequestBody Topic topic){
		topicService.addtopic(topic);
	}
	
	@PutMapping(value="/topics/{id}")
	public void updatetopic(@RequestBody Topic topic,@PathVariable String id) {
		topicService.update(topic,id);
	}


}
