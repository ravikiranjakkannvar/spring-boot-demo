/**
 * 
 */
package exa.com.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Ravikiran J
 *
 */
@Controller
public class ErrorController {

	Logger logger=LoggerFactory.getLogger(ErrorController.class);

	@GetMapping("/error")
	public String errorForm() {
		logger.debug("Error Page with controller Name :",getClass());
		return "error";
	}


}
