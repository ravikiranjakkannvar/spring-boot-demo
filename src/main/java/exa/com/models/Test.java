/**
 * 
 */
package exa.com.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Ravikiran J
 *
 */
@Entity
public class Test {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private int height;
	private float salary;
	private String status;

	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}




}
